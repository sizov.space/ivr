from flask import Flask, render_template, request, send_file, session
from post_text2speech import main_f
import time
import uuid

app = Flask(__name__)

quantity_orders = 0

@app.route('/upload',methods=['POST'])
def upload():
  global quantity_orders
  file = list(request.files.values())[0]
  file.save('resources/audio.wav')
  text = main_f()
  if "Отлично, спасибо за выбор нашего хакатонного магазина." in text:
  	quantity_orders += 1
  return 'send_file("output.mp3", as_attachment=true)'

@app.route('/')
def index():
  return render_template('index.html', quantity_orders=quantity_orders)

@app.route('/output.mp3')
def output():
	time.sleep(5)
	return send_file("output.mp3")

app.run()

